clear;
% work defines
dt = 1e-3;
start_angle = 10;

% define prediction model
F = [1 -dt; 0 1];
B = [dt; 0];

% define observation model
H = [1 0];

% define noise matrixes
Q = [1e-3 0; 0 1e-6];
R = [500];

% initial states
X = [start_angle; 0];
P = 100 * eye(2);

% form dataset
NUM = 10000;
gyro = zeros(1,NUM);
gyro_integral = zeros(1,NUM);
gyro_integral(1) = start_angle;
compl_filter = zeros(1,NUM);
compl_filter(1) = start_angle;

acc = zeros(1,NUM);
x = zeros(2,NUM);

gyro_bias = 1;
for j=1:NUM
    gyro(j) = gyro_bias + 0.001 * randn;
    acc(j) = start_angle + 5 * randn;
end

for i=1:NUM
% prediction step
X = F * X + B * gyro(i);
P = F * P * F' + Q;

% correction step
innov = acc(i) - H * X;
S = H * P * H' + R;
K = P * H' / S;

X = X + K * innov;
P = (eye(2) - K * H) * P;

x(:,i) = X;

if i > 1
    gyro_integral(i) = gyro_integral(i-1) + gyro(i) * dt;
    compl_filter(i) = 0.02 * acc(i) + 0.98 * (compl_filter(i-1) + gyro(i) * dt);
end

end

hold on;
plot(acc);
plot(x(1,:),'r');
%plot(gyro_integral,'g');
plot(compl_filter,'g');
hold off;
%plot(x(2,:));
